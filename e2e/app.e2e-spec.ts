import { MemesPage } from './app.po';

describe('memes App', function() {
  let page: MemesPage;

  beforeEach(() => {
    page = new MemesPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
