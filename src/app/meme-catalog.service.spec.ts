/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { MemeCatalogService } from './meme-catalog.service';

describe('MemeCatalogService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MemeCatalogService]
    });
  });

  it('should ...', inject([MemeCatalogService], (service: MemeCatalogService) => {
    expect(service).toBeTruthy();
  }));
});
