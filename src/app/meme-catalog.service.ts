import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';

import { Meme, Genre } from './models';

const TEST_MEMES = [
  {
    id: 1,
    name: 'One',
    description: 'The first meme',
    url: 'https://cdn.meme.am/cache/instances/folder580/400x400/74407580.jpg',
    genreId: 0
  },
  {
    id: 2,
    name: 'Two',
    description: 'The second meme',
    url: 'https://cdn.meme.am/cache/instances/folder450/250x250/74378450.jpg',
    genreId: 0
  },
  {
    id: 3,
    name: 'Three',
    description: 'The third meme',
    url: 'https://cdn.meme.am/cache/instances/folder736/250x250/74352736.jpg',
    genreId: 1
  },
  {
    id: 4,
    name: 'Four',
    description: 'The fourth meme',
    url: 'https://cdn.meme.am/cache/instances/folder437/250x250/74379437.jpg',
    genreId: 1
  },
  {
    id: 5,
    name: 'Five',
    description: 'The fifth meme',
    url: 'https://cdn.meme.am/cache/instances/folder619/250x250/74388619.jpg',
    genreId: 2
  },

  {
    id: 6,
    name: 'Six',
    description: 'The sixth meme',
    url: 'https://cdn.meme.am/cache/instances/folder580/400x400/74407580.jpg',
    genreId: 0
  },
  {
    id: 7,
    name: 'Seven',
    description: 'The seventh meme',
    url: 'https://cdn.meme.am/cache/instances/folder450/250x250/74378450.jpg',
    genreId: 0
  },
  {
    id: 8,
    name: 'Eight',
    description: 'The eigth meme',
    url: 'https://cdn.meme.am/cache/instances/folder736/250x250/74352736.jpg',
    genreId: 1
  },
  {
    id: 9,
    name: 'Nine',
    description: 'The ninth meme',
    url: 'https://cdn.meme.am/cache/instances/folder437/250x250/74379437.jpg',
    genreId: 1
  },
  {
    id: 10,
    name: 'Ten',
    description: 'The tenth meme',
    url: 'https://cdn.meme.am/cache/instances/folder619/250x250/74388619.jpg',
    genreId: 2
  },
];

const TEST_GENRES: Genre[] = [
  {
    id: 1,
    name: 'Genre 1'
  },
  {
    id: 2,
    name: 'Genre 2'
  },
];

const REQUEST_OPTIONS = new RequestOptions({
  headers: new Headers({
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  })
});

@Injectable()
export class MemeCatalogService {

  constructor(public http: Http) { }

  getGenres(): Promise<Genre[]> {
    return this.http
            .get('http://localhost:49639/api/genres')
            .toPromise()
            .then(res => res.json() as Genre[]);
  }

  getMemes(): Promise<Meme[]> {
    return this.http
            .get('http://localhost:49639/api/images')
            .toPromise()
            .then(res => res.json() as Meme[]);
  }

  getMemesByGenre(genreId: number): Promise<Meme[]> {
    return this.http
            .get(`http://localhost:49639/api/images/?genreId=${genreId}`)
            .toPromise()
            .then(res => res.json() as Meme[]);
  }

  addMeme(meme: Meme): Promise<Meme> {
    return this.http
            .post('http://localhost:49639/api/images', JSON.stringify(meme), REQUEST_OPTIONS)
            .toPromise()
            .then(res => res.json() as Meme);
  }

  removeMeme(memeId: number): Promise<Meme> {
    return this.http
            .delete(`http://localhost:49639/api/images/${memeId}`, REQUEST_OPTIONS)
            .toPromise()
            .then(res => res.json() as Meme);
  }

}
