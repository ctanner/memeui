export interface Meme {
  id?: number;
  name: string;
  description: string;
  url: string;
  genreId: number;
};