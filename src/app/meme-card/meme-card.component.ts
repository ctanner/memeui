import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Meme } from '../models/meme';

@Component({
  selector: 'meme-card',
  templateUrl: './meme-card.component.html',
  styleUrls: ['./meme-card.component.css']
})
export class MemeCardComponent implements OnInit {
  @Input() meme: Meme;
  @Input() genreName: string;
  @Output() remove = new EventEmitter();
  @Output() share = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  onRemove() {
    this.remove.emit(this.meme);
  }

  onShare() {
    this.share.emit(this.meme.url);
  }

}
