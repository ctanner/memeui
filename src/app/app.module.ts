import { MaterialModule } from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { MemeCatalogService } from './meme-catalog.service';

import { AppComponent } from './app.component';
import { MemeCatalogComponent } from './meme-catalog/meme-catalog.component';
import { MemeCardComponent } from './meme-card/meme-card.component';
import { AddMemeDialogComponent } from './add-meme/add-meme-dialog.component';
import { RemoveMemeDialogComponent } from './remove-meme/remove-meme-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    MemeCatalogComponent,
    MemeCardComponent,
    AddMemeDialogComponent,
    RemoveMemeDialogComponent
  ],
  imports: [
    MaterialModule.forRoot(),
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [MemeCatalogService],
  bootstrap: [AppComponent],
  entryComponents: [
    AddMemeDialogComponent,
    RemoveMemeDialogComponent
  ]
})
export class AppModule { }
