import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { MdDialog, MdDialogRef } from '@angular/material';

import { Meme, Genre } from '../models';
import { MemeCatalogService } from '../meme-catalog.service';

import { AddMemeDialogComponent } from '../add-meme/add-meme-dialog.component';
import { RemoveMemeDialogComponent } from '../remove-meme/remove-meme-dialog.component';

@Component({
  selector: 'meme-catalog',
  templateUrl: './meme-catalog.component.html',
  styleUrls: ['./meme-catalog.component.css']
})
export class MemeCatalogComponent implements OnInit {
  genres: Genre[];
  memes: Meme[];

  constructor(
    public catalogService: MemeCatalogService,
    public dialog: MdDialog
  ) { }

  ngOnInit() {
    this.catalogService.getGenres().then(genres => this.genres = genres);
    this.showAll();
  }

  showAll() {
    this.catalogService.getMemes().then(memes => this.memes = memes);
  }

  showForGenre(genreId: number) {
    this.catalogService.getMemesByGenre(genreId).then(memes => this.memes = memes);
  }

  openAddDialog() {
    let dialogRef = this.dialog.open(AddMemeDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      if(typeof result !== 'undefined') {
        this.catalogService.addMeme(result).then(meme => this.memes.push(meme));
      }
    });
  }

  openRemoveDialog(meme: Meme) {
    let dialogRef = this.dialog.open<RemoveMemeDialogComponent>(RemoveMemeDialogComponent);

    dialogRef.componentInstance.meme = meme;

    dialogRef.afterClosed().subscribe(confirmed => {
      if (confirmed === true) {
        this.catalogService.removeMeme(meme.id).then(_ => {
          this.memes = this.memes.filter(m => m.id !== meme.id);
        });
      }
    });
  }

  onShareMeme(url: string) {
    window.prompt('Copy to clipboard: Ctrl+C, Enter', url);
  }

  getGenreName(meme: Meme): string {
    if(this.genres) {
      let genre = this.genres.filter(g => g.id === meme.genreId)[0];

      return genre.name;
    }

    return '';
  }
}
