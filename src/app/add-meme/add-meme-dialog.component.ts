import { Component, OnInit } from '@angular/core';
import { MdDialogRef } from '@angular/material';
import { Observable } from 'rxjs/Observable';

import { Meme, Genre } from '../models';
import { MemeCatalogService } from '../meme-catalog.service';

@Component({
  selector: 'add-meme-dialog',
  templateUrl: './add-meme-dialog.component.html',
  styleUrls: ['./add-meme-dialog.component.css']
})
export class AddMemeDialogComponent implements OnInit {
  meme: Meme;
  genres: Genre[];

  constructor(
      public dialogRef: MdDialogRef<AddMemeDialogComponent>,
      public catalogService: MemeCatalogService
      ) { }

  ngOnInit() {
      this.catalogService.getGenres().then(genres => this.genres = genres);
      this.meme = {name: null, description: null, url: null, genreId: null};
  }

  onSubmit() {
      this.dialogRef.close(this.meme);
  }
}