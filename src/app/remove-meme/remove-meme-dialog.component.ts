import { Component, OnInit, Input } from '@angular/core';
import { MdDialogRef } from '@angular/material';

import { Meme, Genre } from '../models';
import { MemeCatalogService } from '../meme-catalog.service';

@Component({
  selector: 'remove-meme-dialog',
  templateUrl: './remove-meme-dialog.component.html',
  styleUrls: ['./remove-meme-dialog.component.css']
})
export class RemoveMemeDialogComponent implements OnInit {
    @Input() meme: Meme;

    constructor(
        public dialogRef: MdDialogRef<RemoveMemeDialogComponent>,
        public catalogService: MemeCatalogService
        ) { }

    ngOnInit() {
    }

    cancel() {
        this.dialogRef.close(false);
    }

    confirm() {
        this.dialogRef.close(true);
    }

}